package edu.calvin.cs262.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class PlayerActivity extends Activity {
	
	
	private static final String TAG = PlayerActivity.class.getSimpleName();
	private static final String SERVICE_URI = "2602:306:338d:4580:9810:b82f:4407:33cd";
    private static DefaultHttpClient httpClient = new DefaultHttpClient();
    
    private TextView textView = (TextView)findViewById(R.id.myText);
   

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
                
        DownloadWebPageTask task = new DownloadWebPageTask();
        task.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    

    /**
     * This method requests a single Player object from the server in JSON
     * format.
     * 
     * @param i
     *            the index of the player to retrieve
     * @return a string representation of the retrieved player
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    private String retrievePlayer(int i) throws ClientProtocolException, IOException, JSONException {
    	String uri = SERVICE_URI + "/" + i;
    	HttpGet request = new HttpGet(uri);
    	request.addHeader("Accept", "application/json");
    	HttpResponse response = httpClient.execute(request);
    	Log.i(TAG, "Sent GET to: " + uri);
    	return "Player" + i + ":\n\t"
    		+ Player.parseJsonObject(readMessage(response.getEntity().getContent())) + "\n";
    }

    /*
     * This utility method reads the HTTP message content from the given stream
     * and returns it as a string.
     */
    private static String readMessage(InputStream instream) throws IOException {
    	StringBuilder result = new StringBuilder();
    	BufferedReader r = new BufferedReader(new InputStreamReader(instream));
    	String line;
    	while ((line = r.readLine()) != null) {
    		result.append(line);
    	}
    	instream.close();
    	Log.i(TAG, "Read HTTP message: " + result.toString());
    	return result.toString();
    }
    
 // This asynchronous task places the HTTP requests on a separate thread.
 // It is based on
 // http://www.vogella.com/articles/AndroidBackgroundProcessing/article.html
 class DownloadWebPageTask extends AsyncTask<String, Void, String> {
 	@Override
 	protected String doInBackground(String... urls) {
 		return presentPlayersAsText();
 		// presentPlayersAsListView();
 	}
 	@Override
 	protected void onPostExecute(String result) {
 		textView.setText(result);
 	}
  
 /*
  * This method presents the retrieved player in a simple text format.
  * 
  * To use it, the main layout must include a TextView element. To remove the
  * text view, comment this code out and comment out the TextView element.
  */
 	private String presentPlayersAsText() {
 		try {
 			return retrievePlayer(1);
 		} catch (Exception e) {
 			return "error: " + e.getMessage();
 		}
 	}
 }

}
