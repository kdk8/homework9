package edu.calvin.cs262.client;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * Stores information on players - The basic structure is shared with the
 * server8 project (the web service). This class doesn't include methods for
 * active-record database access but does implement its own de-marshalling
 * methods because Android doesn't support JAXB.
 * 
 * @author kvlinden
 * @version Fall, 2012
 */
public class Player {

	private static final String TAG = Player.class.getSimpleName();

	private int id;
	private String name, emailAddress;

	public Player() {
		this(0, "John", "john@somewhere.something");
	}

	public Player(String name, String emailAddress) {
		this(-1, name, emailAddress);
	}

	public Player(int id, String name, String emailAddress) {
		this.id = id;
		this.name = name;
		this.emailAddress = emailAddress;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public String toString() {
		return getId() + ", " + getName() + ", " + getEmailAddress();
	}

	/**
	 * This utility method creates a JSON representation of the given player.
	 * 
	 * This method could be an instance method, but is implemented as a static
	 * method to be consistent with the other field and table utility methods.
	 * 
	 * @param player
	 * @return
	 * @throws JSONException
	 */
	public static JSONObject createJsonObject(Player player) throws JSONException {
		JSONObject result = new JSONObject();
		if (player.getId() >= 0) {
			result.put("id", player.getId());
		}
		result.put("name", player.getName());
		result.put("emailAddress", player.getEmailAddress());
		return result;
	}

	/**
	 * This utility method parses a JSON representation of a list of players as
	 * produced by server8. It assumes that the ID exists, but the other fields
	 * are optional.
	 * 
	 * This method could be a new constructor, but is implemented as a static
	 * method to be consistent with the other field and table utility methods.
	 * 
	 * @param message
	 *            the JSON message string to be parsed
	 * @return a list of player objects
	 * @throws JSONException
	 */
	public static List<Player> parseJsonObjects(String message) throws JSONException {
		List<Player> result = new ArrayList<Player>();
		JSONObject jsonObject = new JSONObject(message);
		JSONArray players = jsonObject.getJSONArray("players");
		for (int i = 0; i < players.length(); i++) {
			result.add(parseJsonObject(players.getString(i)));
		}
		Log.i(TAG, "Parsed message: " + message + " to: " + players);
		return result;
	}
	
	/**
	 * This utility method parses a JSON representation of a single player as
	 * produced by server8. It assumes that the ID exists, but the other fields
	 * are optional.
	 * 
	 * This method could be a new constructor, but is implemented as a static
	 * method to be consistent with the other field and table utility methods.
	 * 
	 * @param message
	 *            the JSON message string to be parsed
	 * @return a player object
	 * @throws JSONException
	 */
	public static Player parseJsonObject(String message) throws JSONException {
		JSONObject jsonObject = new JSONObject(message);
		Player result = new Player(jsonObject.getInt("id"),
				getAttributeIfExists(jsonObject, "name"), getAttributeIfExists(jsonObject,
						"emailAddress"));
		Log.i(TAG, "Parsed message: " + message + " to: " + result);
		return result;
	}

	/*
	 * This utility method gets the value of a JSON text field in the given JSON
	 * object or "" if the field doesn't exit.
	 */
	private static String getAttributeIfExists(JSONObject jsonObject, String fieldName)
			throws JSONException {
		if (jsonObject.has(fieldName)) {
			return jsonObject.getString(fieldName);
		} else {
			return "";
		}
	}

}